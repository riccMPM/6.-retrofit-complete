package hindidictionary.bdrulez.com.retrofitexample.util;



public class FunctionsUtils {

    public static String validateNetworkResponse(int statusCode, String entityDesc, String httpMethod) {
        String actionName;
        String extraMsg = "";
        String result = "No se puede";

        switch (httpMethod) {
            case "GET":
                actionName = " sincronizar/obtener ";
                break;
            case "POST":
                actionName = " insertar/agregar ";
                break;
            case "PUT":
                actionName = " actualizar ";
                break;
            case "DELETE":
                actionName = " eliminar ";
                break;
            case "LOGIN":
                actionName = " ";
                break;
            case "AUTH_WORKLOG":
                actionName = " autorizar ";
                break;
            case "REPORT":
                actionName = " generar ";
                break;

            default:
                actionName = "";
                break;
        }
        result += actionName + entityDesc;

        if (statusCode == NetworkConstants.TEMP_REDIRECT || statusCode == NetworkConstants.REQ_TIMEOUT) {
            extraMsg = ", su conexión a internet no es óptima";
        } else {
            if (statusCode == NetworkConstants.BAD_REQUEST) {
                if (httpMethod.equals(NetworkConstants.HTTP_GET)) {
                    extraMsg = ", refresque nuevamente";
                } else if (httpMethod.equals(NetworkConstants.HTTP_POST) || httpMethod.equals(NetworkConstants.HTTP_PUT)) {
                    extraMsg = ", la información enviada es inválida";
                } else if (httpMethod.equals(NetworkConstants.HTTP_LOGIN)) {
                    extraMsg = ", Usuario o Contraseña inválida";
                } else if (httpMethod.equals(NetworkConstants.HTTP_AUTH_WORKLOG)) {
                    extraMsg = "";
                }
            } else if (statusCode == NetworkConstants.UNAUTHORIZED) {
                if (httpMethod.equals(NetworkConstants.HTTP_LOGIN))
                    extraMsg = ", Usuario o Contraseña inválida";
                else
                    extraMsg = ", inicie sesión nuevamente";
            } else if (statusCode == NetworkConstants.FORBIDDEN) {
                if (httpMethod.equals(NetworkConstants.HTTP_LOGIN)) {
                    extraMsg = ", excedió el límite de intentos, comuníquese con Service Desk";
                } else {
                    extraMsg = ", no cuenta con los permisos necesarios, comuníquese con el Admin";
                }
            } else if (statusCode == NetworkConstants.NOT_FOUND) {
                extraMsg = ", no se encontro la información solicitada";
            } else if (statusCode == NetworkConstants.SRV_UNAVAILABLE) {
                extraMsg = ", el servicio no está disponible, intente mas tarde";
            } else {
                extraMsg = ", comuníquese con el Admin";
            }
        }
        extraMsg += ". (" + statusCode + ")";

        return result + extraMsg;
    }


}
