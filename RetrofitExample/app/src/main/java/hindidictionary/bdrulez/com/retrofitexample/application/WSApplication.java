package hindidictionary.bdrulez.com.retrofitexample.application;

import android.app.Application;

import hindidictionary.bdrulez.com.retrofitexample.rest.RestClient;


/**
 * Created by Lenovo on 16/01/2017.
 */

public class WSApplication extends Application {

    private static RestClient restAdmin;

    @Override
    public void onCreate() {
        super.onCreate();

        restAdmin = new RestClient("", "");


    }


    public static RestClient getRestAdmin() {
        return restAdmin;
    }


}
