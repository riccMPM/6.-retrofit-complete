package hindidictionary.bdrulez.com.retrofitexample.model;

import java.util.List;


/**
 * Created by Riccardo Mija on 14/09/2018.
 */

public class ClinicaResponse {

    private List<Sede> sedeList;
    private List<SedePrevencion> prevencionList;

    public List<Sede> getSedeList() {
        return sedeList;
    }

    public void setSedeList(List<Sede> sedeList) {
        this.sedeList = sedeList;
    }

    public List<SedePrevencion> getPrevencionList() {
        return prevencionList;
    }

    public void setPrevencionList(List<SedePrevencion> prevencionList) {
        this.prevencionList = prevencionList;
    }


}
