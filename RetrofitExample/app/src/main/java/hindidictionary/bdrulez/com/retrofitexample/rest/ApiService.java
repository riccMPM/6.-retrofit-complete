package hindidictionary.bdrulez.com.retrofitexample.rest;


import hindidictionary.bdrulez.com.retrofitexample.model.ClinicaResponse;
import hindidictionary.bdrulez.com.retrofitexample.model.Sede;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Lenovo on 14/09/2018.
 */
public interface ApiService {



    /* Metodos WS */

    @GET("syncSedes")
    Call<ClinicaResponse> syncClinica();

    @POST("postClinica")
    Call<Sede> postSede(@Body Sede sede);
}
