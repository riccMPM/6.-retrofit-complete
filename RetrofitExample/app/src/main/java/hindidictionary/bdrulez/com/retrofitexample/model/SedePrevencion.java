package hindidictionary.bdrulez.com.retrofitexample.model;


public class SedePrevencion {

    private Long idSedePrevencion;
    private Long idSede;
    private String nombre;
    private String latitud;
    private String longitud;

    public SedePrevencion() {
    }

    public Long getIdSedePrevencion() {
        return idSedePrevencion;
    }

    public void setIdSedePrevencion(Long idSedePrevencion) {
        this.idSedePrevencion = idSedePrevencion;
    }

    public Long getIdSede() {
        return idSede;
    }

    public void setIdSede(Long idSede) {
        this.idSede = idSede;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

}
