package hindidictionary.bdrulez.com.retrofitexample.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import hindidictionary.bdrulez.com.retrofitexample.application.WSApplication;
import hindidictionary.bdrulez.com.retrofitexample.rest.ApiService;

public class BaseActivity extends AppCompatActivity {

    public ApiService apiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = WSApplication.getRestAdmin().getApiService();

    }
}
