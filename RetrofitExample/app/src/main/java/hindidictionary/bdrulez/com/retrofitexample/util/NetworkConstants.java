package hindidictionary.bdrulez.com.retrofitexample.util;

/**
 * Created by Lenovo on 27/10/2016.
 */

public class NetworkConstants {
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZED = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    //public static final int INT_SRV_ERROR = 500;
    //public static final int NOT_IMPLEMENTED = 501;
    public static final int SRV_UNAVAILABLE = 503;
    //Valores considerados como conexion inestable/deficiente
    public static final int TEMP_REDIRECT = 307;
    public static final int REQ_TIMEOUT = 408;


    //Http Method

    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_PUT = "PUT";
    public static final String HTTP_DELETE = "DELETE";
    public static final String HTTP_LOGIN = "LOGIN";
    //public static final String HTTP_REPORT = "REPORT";
    public static final String HTTP_AUTH_WORKLOG = "AUTH_WORKLOG";
}
