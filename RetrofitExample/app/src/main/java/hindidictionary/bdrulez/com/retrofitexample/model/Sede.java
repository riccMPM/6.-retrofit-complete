package hindidictionary.bdrulez.com.retrofitexample.model;

import java.io.Serializable;

/**
 * Created by Riccardo Mija on 12/04/2018.
 */

public class Sede implements Serializable {

    private static final long serialVersionUID = 87996564786747166L;

    private Long idSede;
    private Long idPadre;
    private String nombreSede;
    private String direccionSede;
    private String descripcionSede;
    private String numeroSede;
    private String urlImagen;
    private String urlSede;
    private Boolean activo;
    private Boolean esOncoSalud;
    private String color;
    private String latitud;
    private String longitud;
    private String urlReserva;
    private boolean esPrevencion;
    private Boolean esCentroPrevencionTratamiento;

    public Sede() {
    }


    public Long getIdSede() {
        return idSede;
    }

    public void setIdSede(Long idSede) {
        this.idSede = idSede;
    }

    public Long getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Long idPadre) {
        this.idPadre = idPadre;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDireccionSede() {
        return direccionSede;
    }

    public void setDireccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
    }

    public String getDescripcionSede() {
        return descripcionSede;
    }

    public void setDescripcionSede(String descripcionSede) {
        this.descripcionSede = descripcionSede;
    }

    public String getNumeroSede() {
        return numeroSede;
    }

    public void setNumeroSede(String numeroSede) {
        this.numeroSede = numeroSede;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getUrlSede() {
        return urlSede;
    }

    public void setUrlSede(String urlSede) {
        this.urlSede = urlSede;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getEsOncoSalud() {
        return esOncoSalud;
    }

    public void setEsOncoSalud(Boolean esOncoSalud) {
        this.esOncoSalud = esOncoSalud;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getUrlReserva() {
        return urlReserva;
    }

    public void setUrlReserva(String urlReserva) {
        this.urlReserva = urlReserva;
    }

    public Boolean getEsCentroPrevencionTratamiento() {
        return esCentroPrevencionTratamiento;
    }

    public void setEsCentroPrevencionTratamiento(Boolean esCentroPrevencionTratamiento) {
        this.esCentroPrevencionTratamiento = esCentroPrevencionTratamiento;
    }

    public boolean isEsPrevencion() {
        return esPrevencion;
    }

    public void setEsPrevencion(boolean esPrevencion) {
        this.esPrevencion = esPrevencion;
    }
}