package hindidictionary.bdrulez.com.retrofitexample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hindidictionary.bdrulez.com.retrofitexample.R;
import hindidictionary.bdrulez.com.retrofitexample.model.Sede;


/**
 * Created by riccMP on 14/09/2018.
 */

public class SedeAdapter extends RecyclerView.Adapter<SedeAdapter.ViewHolder> {


    private Context mContext;
    private List<Sede> lstSede;

    public SedeAdapter(Context context) {
        this.mContext = context;
        this.lstSede = new ArrayList<>();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clinica, parent, false);

        return new ViewHolder(v, mContext);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        final Sede sede = lstSede.get(position);
        viewHolder.tviNombreSede.setText(sede.getNombreSede());

        Picasso.get()
                .load(sede.getUrlImagen())
                .placeholder(R.drawable.img_clinica)
                .into(viewHolder.iviClinica);


    }

    public void setList(List<Sede> lstSede) {

        this.lstSede = lstSede;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.lstSede.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tviNombreSede;
        ImageView iviClinica;
        Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            tviNombreSede = view.findViewById(R.id.tviNombreSede);
            iviClinica = view.findViewById(R.id.iviClinica);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "Click en la clinica " + tviNombreSede.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }


}