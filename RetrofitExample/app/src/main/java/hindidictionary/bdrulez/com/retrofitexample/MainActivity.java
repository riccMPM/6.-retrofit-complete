package hindidictionary.bdrulez.com.retrofitexample;

import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import hindidictionary.bdrulez.com.retrofitexample.activity.BaseActivity;
import hindidictionary.bdrulez.com.retrofitexample.adapter.SedeAdapter;
import hindidictionary.bdrulez.com.retrofitexample.model.ClinicaResponse;
import hindidictionary.bdrulez.com.retrofitexample.model.Sede;
import hindidictionary.bdrulez.com.retrofitexample.util.Constants;
import hindidictionary.bdrulez.com.retrofitexample.util.FunctionsUtils;
import hindidictionary.bdrulez.com.retrofitexample.util.NetworkConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private SwipeRefreshLayout sreSede;
    private RecyclerView rviSedes;

    private SedeAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rviSedes = findViewById(R.id.rviSedes);
        sreSede = findViewById(R.id.sreSede);

        sreSede.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refrescarSedes();
            }
        });

        configurarClinicas();
        sincronizarSedes();


    }

    private void sincronizarSedes() {

        mostrarLoader(true);


        //Instanciamos clase call
        Call<ClinicaResponse> syncSedeResponse = apiService.syncClinica();

        //Consumir servicio
        syncSedeResponse.enqueue(new Callback<ClinicaResponse>() {
            @Override
            public void onResponse(Call<ClinicaResponse> call, Response<ClinicaResponse> response) {

                //Verificar que la peticion sea correcta
                String msgResultado = response.message();


                if (msgResultado.equals(Constants.RESPONSE_OK_GET)) {

                    List<Sede> lstSedes = response.body().getSedeList();
                    adapter.setList(lstSedes);


                } else {
                    String networkResponse = FunctionsUtils.validateNetworkResponse(response.raw().code(), " las Clinicas ", NetworkConstants.HTTP_GET);
                    Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                }

                mostrarLoader(false);

            }

            @Override
            public void onFailure(Call<ClinicaResponse> call, Throwable t) {
                String networkResponse = FunctionsUtils.validateNetworkResponse(NetworkConstants.TEMP_REDIRECT, " las Clinicas ", NetworkConstants.HTTP_GET);

                Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                mostrarLoader(false);

            }
        });

    }


    private void refrescarSedes() {

        mostrarLoader(true);


        //Instanciamos clase call
        Call<ClinicaResponse> syncSedeResponse = apiService.syncClinica();

        //Consumir servicio
        syncSedeResponse.enqueue(new Callback<ClinicaResponse>() {
            @Override
            public void onResponse(Call<ClinicaResponse> call, Response<ClinicaResponse> response) {

                //Verificar que la peticion sea correcta
                String msgResultado = response.message();


                if (msgResultado.equals(Constants.RESPONSE_OK_GET)) {

                    List<Sede> lstSedes = response.body().getSedeList();
                    lstSedes.remove(0);
                    lstSedes.remove(4);
                    adapter.setList(lstSedes);


                } else {
                    String networkResponse = FunctionsUtils.validateNetworkResponse(response.raw().code(), " las Clinicas ", NetworkConstants.HTTP_GET);
                    Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                }

                mostrarLoader(false);

            }

            @Override
            public void onFailure(Call<ClinicaResponse> call, Throwable t) {
                String networkResponse = FunctionsUtils.validateNetworkResponse(NetworkConstants.TEMP_REDIRECT, " las Clinicas ", NetworkConstants.HTTP_GET);

                Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                mostrarLoader(false);

            }
        });

    }


    private void postSede() {

        mostrarLoader(true);


        //Instanciamos clase call
        Sede sede = new Sede();
        sede.setDescripcionSede("Sede principal");
        sede.setNombreSede("Sede Cielo");

        Call<Sede> sedeCall = apiService.postSede(sede);

        //Consumir servicio
        sedeCall.enqueue(new Callback<Sede>() {
            @Override
            public void onResponse(Call<Sede> call, Response<Sede> response) {

                //Verificar que la peticion sea correcta
                String msgResultado = response.message();


                if (msgResultado.equals(Constants.RESPONSE_OK_GET)) {

                    Sede sede = response.body();


                } else {
                    String networkResponse = FunctionsUtils.validateNetworkResponse(response.raw().code(), " la Sede ", NetworkConstants.HTTP_POST);
                    Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                }

                mostrarLoader(false);

            }

            @Override
            public void onFailure(Call<Sede> call, Throwable t) {
                String networkResponse = FunctionsUtils.validateNetworkResponse(NetworkConstants.TEMP_REDIRECT, " la Sede ", NetworkConstants.HTTP_POST);

                Toast.makeText(getBaseContext(), networkResponse, Toast.LENGTH_SHORT).show();
                mostrarLoader(false);

            }
        });

    }

    private void configurarClinicas() {
        adapter = new SedeAdapter(this);
        layoutManager = new LinearLayoutManager(this);

        rviSedes.setHasFixedSize(true);
        rviSedes.setLayoutManager(layoutManager);
        rviSedes.setAdapter(adapter);
    }


    public void mostrarLoader(final boolean showLoader) {
        sreSede.post(new Runnable() {
            @Override
            public void run() {
                sreSede.setRefreshing(showLoader);
            }
        });
    }
}
